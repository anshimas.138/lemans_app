import { Component, OnInit, HostListener } from '@angular/core';
import { GetHelmetDataService} from 'src/app/services/get-helmet-data.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {
  data;
  hits;
  helmet_name;
  price_start;
  price_end;
  brand_name;
  image;
  dataArrayfiltered;
  clicked;
  dataArray =[];
  constructor(public helmetDataService: GetHelmetDataService){
  }
  ngOnInit() {
    this.loadHelmets();
  }
  title = 'ProductFilter-App';
  loadHelmets() {
    this.clicked=false;
    this.helmetDataService.loadData().subscribe(data=>{
     this.data = data;
     this.hits = this.data.result.hits;
     for(var i=0;i<this.hits.length;i++){
      this.helmet_name = this.data.result.hits[i].name; 
      this.price_start = this.data.result.hits[i].partSummary.priceRanges.retail.start;
      this.price_end = this.data.result.hits[i].partSummary.priceRanges.retail.end;
      this.brand_name = this.data.result.hits[i].brand.name;
    
      for(var j=0;j<this.data.result.hits[i].media.length;j++){
        this.image = this.data.result.hits[i].media[j].url;
      }
      this.dataArray.push({
        helmet_name: this.helmet_name,
        brand_name: this.brand_name,
        price_start: this.price_start,
        price_end: this.price_end,
        image_url: `https://asset.lemansnet.com/${this.image}.png`
       });
     }
     
    }); 
  } 
  
 filter_click(filterMin, filterMax){
 var isValid = (Number(filterMin) < Number(filterMax) && filterMin!==NaN && filterMax!==NaN && Math.sign(Number(filterMin))!==-1 && Math.sign(Number(filterMax))!==-1)
  if(filterMin==="" || filterMax===""){
    window.alert("Please enter a value");
  }
  else if(isValid){
  this.dataArrayfiltered = this.dataArray.filter(t=>t.price_end >= filterMin && t.price_end<= filterMax);
   this.clicked =true;
  return this.dataArrayfiltered;
  }
  else if(!isValid){
    if(Number(filterMin) > Number(filterMax)&& Math.sign(Number(filterMin))!==-1 && Math.sign(Number(filterMax))!==-1){
    window.alert("Minimum price cannot exceed Maximum price");
    return;
    }
    else if(Number(filterMin)==Number(filterMax)){
      window.alert("Please enter different values");
      return;
    }
    else if(Math.sign(Number(filterMin))==-1||Math.sign(Number(filterMax))==-1){
      window.alert("Please enter a valid number");
      return;
    }
    else (filterMin===NaN||filterMax===NaN)
      window.alert("Please enter a valid number");
      return;
       
  }
  
}

  } 
  

