import { TestBed, inject } from '@angular/core/testing';

import { GetHelmetDataService } from './get-helmet-data.service';

describe('GetHelmetDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetHelmetDataService]
    });
  });

  it('should be created', inject([GetHelmetDataService], (service: GetHelmetDataService) => {
    expect(service).toBeTruthy();
  }));
});
